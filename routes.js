module.exports = function(app) {
  var r = app.get('r');
  var router = app.get('router');

  router.post('/send', function(req,res,next) {
    var message = req.body.message;
    r.db('rethinkdb_tutorial').table('messages')
      .insert({
        message: message,
        date: new Date()
      })
      .run()
      .then(function(result) {
        res.send('Message sent!');
      })
      .error(function(err) {
        res.status(500).send(err);
      })
  });

  router.get('/all', function(req,res,next) {
    r.db('rethinkdb_tutorial').table('messages')
      .orderBy({index: r.desc('date')})
      .run()
      .then(function(result) {
        res.send(result);
      })
      .error(function(err) {
        res.status(500).send(err);
      })
  })

  return router;
}
