var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io')(server);
var r = require('rethinkdbdash')();
var async = require('async');
var bodyParser = require('body-parser');

//A nicely formatted waterfall function in async.
async.waterfall([
  function(finished) {
    r.dbList().contains('rethinkdb_tutorial')
      .run()
      .then(function(result) {
  		finished(null, result)
      })
      .error(function(err) {
        finished(err);
      })
  },
  function(dbExists, finished) {
    if (dbExists == false) {
      r.dbCreate('rethinkdb_tutorial')
        .run()
        .then(function() {
          finished(null);
        })
        .error(function(err) {
          finished(err);
        });
    } else {
      finished(null);
    }
  },
  function(finished) {
    r.db('rethinkdb_tutorial').tableList().contains('messages')
      .run()
      .then(function(result) {
        finished(null,result);
      })
      .error(function(err) {
        finished(err);
      });
  },
  function(tableExists, finished) {
    if (tableExists == false) {
      r.db('rethinkdb_tutorial').tableCreate('messages')
        .run()
        .then(function(result) {
          finished(null, false);
        })
        .error(function(err) {
          finished(err);
        })
    } else {
      finished(null, true);
    }
  },
  function(tableIndexed, finished) {
    if (tableIndexed == false) {
      r.db('rethinkdb_tutorial').table('messages')
      .indexCreate('date')
        .run()
      	.then(function(result) {
    	    finished(null);
		    })
        .error(function(err) {
          finished(err);
        })
    } else {
      finished(null);
    }
  }
], function(err) {
  if (err) throw err;
  r.db('rethinkdb_tutorial').table('messages')
    .changes()
    .run()
    .then(function(cursor) {
      cursor.each(function(err, result) {
        console.log(result);
        io.emit('new_message', result);
      });
    });
});

//Set Express' dependencies.
app.set('r', r);
app.set('router', express.Router());

app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded
//Route to the messages folder.
app.use('/message', require('./routes.js')(app));

//Serve the static html page.
app.use(express.static('./public/'));
app.get('/', function(req,res,next) {
  res.sendFile('./public/index.html');
});

//Create socket.io event
io.on('connection', function(socket) {
  console.log('New client connected');
})

//Serve the content on a port.
var PORT = 3000;
server.listen(PORT);
console.log("RethinkDB Tutorial running on " + PORT);
