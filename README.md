# Creating a Simple Chat Application with Rethinkdb

In this tutorial, you will be learning how to:

- Setup a nitrous.io container then deploy and configure a Node.js application.
- Create a *tiny* RESTful API to send and receive data from RethinkDB.
- Utilize socket.io to provide real time events in a web app.
- Use the RethinkDB `.changes()` function to seamlessly deliver realtime events.

## Why use `rethinkdbdash`?

- Connection pooling for cleaner and more maintainable code.
- reduces redundancies with potential multiple connections.
- Easy to scale with multiple RethinkDB nodes for high availability.
- Very easy to refactor existing code.

## Setup

Before you begin, **you should have a nitrous.io account**. You can [signup here](http://nitrous.io).

You need to select the Node.js container upon registration.

![nitrous container](https://nitrous-community.s3.amazonaws.com/images/0026_01.png)

After configuring your container you need to [configure rethinkdb](https://community.nitrous.io/tutorials/setting-up-rethinkdb-on-nitrous) to run on the container as a service.

If you'd like to run a fully functional demo of this tutorial you can clone it and run it by running the following commands in your container:

``` shell
git clone https://bitbucket.org/ccates/rethinkdbtutorial && cd rethinkdbtutorial && npm i && node app.js
```

You can run commands in nitrous at the bottom of the web browser.

## Configuring the database

Our desired schema is illustrated in the following image:

![database schema](https://nitrous-community.s3.amazonaws.com/images/0026_02.png)

Before we can create this database structure, we need to understand how to generate it. A good practice for your RethinkDB databases is to automatically generate the database, their tables, and their indexes programmatically.

In RethinkDB, checking in the database whether or not a database exists requires you to use the `.contains()`function on the `.dbList()`function.

``` javascript
r.dbList().contains('rethinkdb_tutorial')
  .run()
  .then(function(result) {
    return result;
  });
```

The above would return `false` if there is no database called `rethinkdb_tutorial`, and, `true`if there is a database.

You can also do something similar for tables, except with `.tableList()`, you would also have to identify the database with `.db()`too.

``` javascript
r.db('rethinkdb_tutorial').tableList().contains('messages')
  .run()
  .then(function(result) {
    return result;
  });
```

Now that you understand how to identify when a database or table does or does not exists, let's start creating them. In the following example I will be using the `async` package. You can review the package [here](https://github.com/caolan/async). Basically, we will be asynchronously creating databases and then tables utilizing async's `waterfall` function passing the data to the next callback asynchronously. We **only** proceed to creating database, tables or indexes after knowing it has **not** been created.

``` javascript
async.waterfall([
  function(finished) {
    r.dbList().contains('rethinkdb_tutorial')
      .run()
      .then(function(result) {
  		finished(null, result)
      })
      .error(function(err) {
        finished(err);
      })
  },
  function(dbExists, finished) {
    if (dbExists == false) {
      r.dbCreate('rethinkdb_tutorial')
        .run()
        .then(function() {
          finished(null);
        })
        .error(function(err) {
          finished(err);
        });
    } else {
      finished(null);
    }
  },
  function(finished) {
    r.db('rethinkdb_tutorial').tableList().contains('messages')
      .run()
      .then(function(result) {
        finished(null,result);
      })
      .error(function(err) {
        finished(err);
      });
  },
  function(tableExists, finished) {
    if (tableExists == false) {
      r.db('rethinkdb_tutorial').tableCreate('messages')
        .run()
        .then(function(result) {
          finished(null, false);
        })
        .error(function(err) {
          finished(err);
        })
    } else {
      finished(null, true);
    }
  },
  function(tableIndexed, finished) {
    if (tableIndexed == false) {
      r.db('rethinkdb_tutorial').table('messages').indexCreate('date')
        .run()
      	.then(function(result) {
    	  finished(null);
		})
        .error(function(err) {
          finished(err);
        })
    } else {
      finished(null);
    }
  }
], function(err) {
  if (err) throw err;
});
```

The above will generate a database called `rethinkdb_tutorial`, a table called `messages` with an index `date`. Note that this script can run safely many times without errors. If you were to simply run `.tableCreate('messages')` or `.dbCreate('rethinkdb_tutorial')` twice, this in turn would create an error. This is because this script only queries the database whereas commands like `.tableCreate` creates a table, and if we try to re-create a table that already exists, that is going to cause an error.

## Creating a RESTful API

Despite this being a realtime web application, we still need a REST service to `GET` and `POST` data to the server. In this tutorial we will be using Express.

Imagine our routes like this, the server being Express.

![routes setup](https://nitrous-community.s3.amazonaws.com/images/0026_03.png)

### On the backend

First we need to create a `POST`request. Let's call it `/message/send`. Inside the `POST` route, we will need to do an `.insert()`on the table `messages` in the `rethinkdb_tutorial` database.

``` javascript
router.post('/send', function(req,res,next) {
    var message = req.body.message;
    r.db('rethinkdb_tutorial').table('messages')
      .insert({
        message: message,
        date: new Date()
      })
      .run()
      .then(function(result) {
        res.send('Message sent!');
      })
      .error(function(err) {
        res.status(500).send('Internal Server Error');
      })
  })
```

Second we need to create a `GET` request. Let's call it `/message/all`. Inside the `GET`route, we will need to simply select all the messages by calling `.table('messages')` and then ordering it by it's `date` index respectively.

``` javascript
router.get('/all', function(req,res,next) {
    r.db('rethinkdb_tutorial').table('messages')
      .orderBy({index: r.asc('date')})
      .run()
      .then(function(result) {
        res.send(result);
      })
      .error(function(err) {
        res.status(500).send('Internal Server Error');
      })
  })
```

*Note: in the example script. We chain the router to the `/message` route with `express.use()`.*

### On the frontend

For simplicities sake, we will be using jQuery to `GET` and `POST` data.

First we will create a POST request using jQuery's `$.post()`function.

``` javascript
$.post('/message/send', {message: message}, function(result) {
  console.log(result);
});
```

Then we will create a GET request using jQuery's `$.get()`function. We will also `.map()`the data then append it to the html.

``` javascript
$.get('/message/all', function(result) {
  var messages = "";
  result.map(function(obj) {
    messages += "<li>Someone said: " + obj.message + "</li>";
  });
  $('.messages').html(messages);
});
```

Keep in mind we also need a form to send data as well, and, that this should be configured to the jQuery `$.post()`on a click handler.

``` javascript
$('.message-btn').click(function() {
  var val = $('.message-input').val();
  //Do POST request here then clear input
})
```

## Socket.io and RethinkDB's `.changes()`

Now that we've successfully configured a REST service and we can send and receive data. It's time to make utilize RethinkDB for real time interaction. In our case we will be using the `.changes()` function on our RethinkDB table `messages`.

### On the backend

We need to wrap a socket.io event emitter inside the `.changes()`emitter for RethinkDB. We automatically emit each event and send it to the server. We will emit an event called `new_message`.

``` javascript
r.db('rethinkdb_tutorial').table('messages')
  .changes()
  .run()
  .then(function(cursor) {
    cursor.each(function(err, result) {
      console.log(result);
      io.emit('new_message', result);
    });
  });
```

### On the frontend

We need to include the socket.io client in our HTML `https://cdn.socket.io/socket.io-1.3.7.js` or alternatively download it [here](http://socket.io/download/). Then we have to connect to the server via `socket = io.connect();` then we can start receiving events.

You just need to register an `.on()` in order to start receiving real time data.

``` javascript
socket.on('new_message', function(data) {
  data.map(function(d) {
    $('.messages').append('<li>'+d.new_val.message+'</li>');
  })
});
```

**Note that the `.changes()` sends both a `.new_val` and `.old_val` in it's array.**

## Conclusion

If you want to run the full version of the application please check out the [this](https://bitbucket.org/ccates/rethinkdbtutorial) repository.

Congratulations, you now know how to make a real time web application with RethinkDB. I hope that you've learned plenty, and, if you have any questions feel free to message me (@itschriscates).

— Cheers, Chris Cates
